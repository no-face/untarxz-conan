# untarxz-conan

Helper for [Conan.io](https://www.conan.io/) packages to extract `.xz` files.

The package will try to use python3 tarfile. If fail, it will fallback to `lzma`
library or `tar` command.

## How to use

Add the package as a build requirement and import it using `pythonpath` tool.

Example:

```python
from conans import ConanFile, tools

class Example(ConanFile):
    build_requires = "untarxz/0.0.1@noface/testing"

    def build(self):
        ... # download or get file

        with tools.pythonpath(self):
            from untarxz import untarxz
            untarxz(self, example_file)
```



## License

This package is distributed under the [unlicense](http://unlicense.org/) terms (see UNLICENSE.md).

