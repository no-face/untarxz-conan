from conans import ConanFile, tools

import os
from os import path

username     = os.getenv("CONAN_USERNAME", "noface")
channel      = os.getenv("CONAN_CHANNEL", "testing")
package_name = "untarxz"
version      = "0.0.1"

class PackageTest(ConanFile):
    requires = (
         "%s/%s@%s/%s" % (package_name, version, username, channel)
    )

    exports = "hello.txt.tar.xz"

    hello_sha256 = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"

    def test(self):
        f = path.join(self.conanfile_directory, "hello.txt.tar.xz")

        self.output.info("******************************************")

        self.output.info("extracting file: " + f)

        with tools.pythonpath(self):
            from untarxz import untarxz
            untarxz(self, f)

            assert path.isfile("hello.txt")
            tools.check_sha256("hello.txt", self.hello_sha256)


        self.output.info("***************** Ok *********************")
