#!/usr/bin/env python
# -*- coding: utf-8 -*-

def untarxz(conanfile, filename):
    '''Extract xz file'''

    for m in [uncompress_xz_3_3, uncompress_lzma, uncompress_tar]:
        if m(conanfile, filename):
            return

    raise RuntimeError("Failed to decompress file '{}'. Try to install lzma library or use python3.".format(filename))

def uncompress_xz_3_3(conanfile, filename):
    import tarfile

    try:
        with tarfile.open(filename) as f:
            f.extractall('.')

        return True
    except tarfile.ReadError:
         return False

def uncompress_lzma(conanfile, filename):
    try:
        import lzma
    except:
        conanfile.output.info("module 'lzma' not found.")
        return False

    import contextlib
    import tarfile

    with contextlib.closing(lzma.LZMAFile(filename)) as xz:
        with tarfile.open(fileobj=xz) as f:
            f.extractall('.')

    return True

def uncompress_tar(conanfile, filename):
    cmd = "tar xvfJ " + filename
    conanfile.output.info(cmd)

    try:
        conanfile.run(cmd)
        return True
    except:
        return False
