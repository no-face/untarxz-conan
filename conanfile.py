import os
from os import path

from conans import ConanFile

class Recipe(ConanFile):
    name        = "untarxz"
    version     = "0.0.1"
    description = "helper package to decompress xz files"
    license     = "unlicense"
    url         = "https://gitlab.com/no-face/untarxz-conan"  #recipe repo url

    exports     = "*.py"
    build_policy = "missing"

    def package(self):
        self.copy("*.py")

    def package_info(self):
        self.env_info.PYTHONPATH.append(self.package_folder)
